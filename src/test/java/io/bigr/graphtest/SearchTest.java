package io.bigr.graphtest;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test graph searches. Note that we are testing the specific search algorithm(s) 
 * and not the public search() method, as it is just a pointer to one of the above.
 */
public class SearchTest {
	
	protected Graph<Edge> graph;
	protected Vertex source;
	protected Vertex target;
	protected Vertex a;
	protected Vertex b;
	protected Vertex c;
	protected Vertex d;
	protected Vertex e;
	protected Vertex f;
	protected Edge sa;
	protected Edge sa2;
	protected Edge sb;
	protected Edge af;
	protected Edge at;
	protected Edge bc;
	protected Edge bd;
	protected Edge be;
	protected Edge cd;
	protected Edge dc;
	protected Edge ct;
	protected Edge dt;
	protected Edge eb;

	protected HyperGraph<HyperEdge> hypergraph;
	protected Vertex v1;
	protected Vertex v2;
	protected Vertex v3;
	protected Vertex v4;
	protected Vertex v5;
	protected Vertex v6;
	protected Vertex v7;
	protected Vertex v8;
	protected Vertex v9;
	protected Vertex v10;
	protected Vertex v11;
	protected HyperEdge e1;
	protected HyperEdge e2;
	protected HyperEdge e3;
	protected HyperEdge e4;
	protected HyperEdge e5;
	protected HyperEdge e6;
	protected HyperEdge e7;
	protected HyperEdge e8;
	protected HyperEdge e9;
	protected Set<Vertex> terminals;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	private void setupGraph() {
		graph = new Graph<Edge>();
		source = new VertexImpl("Source");
		target = new VertexImpl("Target");
		a = new VertexImpl("A");
		b = new VertexImpl("B");
		c = new VertexImpl("C");
		d = new VertexImpl("D");
		e = new VertexImpl("E");
		f = new VertexImpl("F");
		sa = new EdgeImpl ("sa", source, a);
		sa2 = new EdgeImpl ("sa2", source, a);
		sb = new EdgeImpl ("sb", source, b);
		af = new EdgeImpl ("af", a, f);
		at = new EdgeImpl ("at", a, target);
		bc = new EdgeImpl ("bc", b, c);
		bd = new EdgeImpl ("bd", b, d);
		be = new EdgeImpl ("be", b, e);
		cd = new EdgeImpl ("cd", c, d);
		dc = new EdgeImpl ("dc", d, c);
		ct = new EdgeImpl ("ct", c, target);
		dt = new EdgeImpl ("dt", d, target);
		eb = new EdgeImpl ("eb", e, b);
		
		graph.addVertex(source);
		graph.addVertex(target);
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);
		graph.addVertex(f);
		
		graph.addEdge(sa);
		graph.addEdge(sa2);
		graph.addEdge(sb);
		graph.addEdge(af);
		graph.addEdge(at);
		graph.addEdge(bc);
		graph.addEdge(bd);
		graph.addEdge(be);
		graph.addEdge(cd);
		graph.addEdge(dc);
		graph.addEdge(ct);
		graph.addEdge(dt);
		graph.addEdge(eb);
	}

	private void setupHyperGraph() {
		hypergraph = new HyperGraph<HyperEdge>();
		v1 = new VertexImpl("v1");
		hypergraph.addVertex(v1);
		v2 = new VertexImpl("v2");
		hypergraph.addVertex(v2);
		v3 = new VertexImpl("v3");
		hypergraph.addVertex(v3);
		v4 = new VertexImpl("v4");
		hypergraph.addVertex(v4);
		v5 = new VertexImpl("v5");
		hypergraph.addVertex(v5);
		v6 = new VertexImpl("v6");
		hypergraph.addVertex(v6);
		v7 = new VertexImpl("v7");
		hypergraph.addVertex(v7);
		v8 = new VertexImpl("v8");
		hypergraph.addVertex(v8);
		v9 = new VertexImpl("v9");
		hypergraph.addVertex(v9);
		v10 = new VertexImpl("v10");
		hypergraph.addVertex(v10);
		v11 = new VertexImpl("v11");
		hypergraph.addVertex(v11);
		terminals = new HashSet<Vertex>();
		terminals.add(v6);
		terminals.add(v9);
		terminals.add(v10);
		e1 = new HyperEdgeImpl("e1").addHead(v1).addTail(v3).addTail(v4).addTail(v5);
		hypergraph.addEdge(e1);
		e2 = new HyperEdgeImpl("e2").addHead(v2).addTail(v3).addTail(v4).addTail(v5);
		hypergraph.addEdge(e2);
		e3 = new HyperEdgeImpl("e3").addHead(v3).addTail(v9);
		hypergraph.addEdge(e3);
		e4 = new HyperEdgeImpl("e4").addHead(v4).addTail(v6).addTail(v8);
		hypergraph.addEdge(e4);
		e5 = new HyperEdgeImpl("e5").addHead(v5).addTail(v7);
		hypergraph.addEdge(e5);
		e6 = new HyperEdgeImpl("e6").addHead(v8).addTail(v10);
		hypergraph.addEdge(e6);
		e7 = new HyperEdgeImpl("e7").addHead(v7).addTail(v10);
		hypergraph.addEdge(e7);
		e8 = new HyperEdgeImpl("e8").addHead(v1).addTail(v3);
		hypergraph.addEdge(e8);
		e9 = new HyperEdgeImpl("e9").addHead(v5).addTail(v11);
		hypergraph.addEdge(e9);
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Solutions are:
	 * SA-AT, SA2-AT, SB-BC-CT, SB-BC-CD-DT, SB-BD-DT, SB-BD-DC-CT
	 */
	@Test
	public final void testSearch() {
		setupGraph();
		List<List<Edge>> result = Search.search(source, target, graph);
		for (List<Edge> r : result) {
			StringBuffer sb = new StringBuffer();
			for (Edge e : r) {
				sb.append(e.getName());
				sb.append("-");
			}
//			System.out.println(sb.toString());
		}
		
		// TODO - set assertions
	}
}
