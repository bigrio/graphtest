package io.bigr.graphtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test basic graph operations
 */
public class GraphTest {
	
	protected Graph<Edge> graph;
	protected Vertex a;
	protected Vertex b;
	protected Vertex c;
	protected Vertex d;
	protected Edge ab;
	protected Edge ac;
	protected Edge ac2;
	protected Edge cd;
	protected Edge bd;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		graph = new Graph<Edge>();
		a = new VertexImpl("a");
		b = new VertexImpl("b");
		c = new VertexImpl("c");
		d = new VertexImpl("d");
		ab = new EdgeImpl("ab", a, b);
		ac = new EdgeImpl("ac", a, c);
		ac2 = new EdgeImpl("ac2", a, c);
		cd = new EdgeImpl("cd", c, d);
		bd = new EdgeImpl("bd", b, d);
	}

	/**
	 * Populate the graph with all defined edges and vertexes
	 */
	protected void setUpGraph() {
		graph = new Graph<Edge>();
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addEdge(ab);
		graph.addEdge(ac);
		graph.addEdge(ac2);
		graph.addEdge(cd);
		graph.addEdge(bd);
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAddEdge() {
		graph.addEdge(ab);
		assertEquals(2, graph.getVertices().size()); // Make sure the vertexes were added
		Set<Edge> edges = graph.getEdges(a);
		assertEquals(1, edges.size()); // There should be only one edge in the graph
		assertEquals(ab, edges.iterator().next()); // ...and it should be ab
		
		setUpGraph();
		assertEquals(5, graph.edges.size());
	}

	@Test
	public final void testRemoveEdge() {
		setUpGraph();
		graph.removeEdge(ab);
		Set<Edge> aEdges = graph.getEdges(a);
		assertEquals(2, aEdges.size()); // We started with 3 a-sourced edges, so removing one leaves 2
		assertFalse(aEdges.contains(ab));
	}

	@Test
	public final void testAddVertex() {
		graph.addVertex(a);
		assertTrue(graph.getVertices().contains(a));
	}

	@Test
	public final void testRemoveVertex() {
		setUpGraph();
		graph.removeVertex(b);
		Set<Vertex> remainingVertexes = graph.getVertices();
		assertEquals(3, remainingVertexes.size()); // We started with 4 vertexes, so removing one leaves 3
		assertFalse(remainingVertexes.contains(b));
		
		Set<Edge> edgesFromB = graph.getEdges(b);
		assertEquals(0, edgesFromB.size()); // Since we removed the b vertex, all edges with b as source should be gone.

		Set<Edge> edgesFromA = graph.getEdges(a);
		assertEquals(2, edgesFromA.size()); // Since we removed the b vertex, all edges with b as target should be gone.
	}

	@Test
	public final void testGetVertexes() {
		setUpGraph();
		assertEquals(4, graph.getVertices().size());
	}

	@Test
	public final void testGetEdges() {
		setUpGraph();
		assertEquals(3, graph.getEdges(a).size());
	}

	@Test
	public final void testGetTargetVertex() {
		setUpGraph();
		assertEquals(b, ab.getHead());
	}

}
