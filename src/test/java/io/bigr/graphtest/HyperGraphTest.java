package io.bigr.graphtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Test basic graph operations
 */
public class HyperGraphTest {
	
	protected HyperGraph<HyperEdge> graph;
	protected Vertex a;
	protected Vertex b;
	protected Vertex c;
	protected Vertex d;
	protected Vertex e;
	protected Vertex f;
	protected HyperEdge ab;
	protected HyperEdge acd;
	protected HyperEdge de;
	protected HyperEdge bef;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		graph = new HyperGraph<HyperEdge>();
		a = new VertexImpl("a");
		b = new VertexImpl("b");
		c = new VertexImpl("c");
		d = new VertexImpl("d");
		d = new VertexImpl("e");
		d = new VertexImpl("f");
		ab = new HyperEdgeImpl("ab").addHead(a).addTail(b);
		acd = new HyperEdgeImpl("acd").addHead(a).addTail(c).addTail(d);
		de = new HyperEdgeImpl("de").addHead(d).addTail(e);
		bef = new HyperEdgeImpl("bef").addHead(b).addHead(e).addTail(f);
	}

	/**
	 * Populate the graph with all defined edges and vertexes
	 */
	protected void setUpGraph() {
		graph = new HyperGraph<HyperEdge>();
		graph.addVertex(a);
		graph.addVertex(b);
		graph.addVertex(c);
		graph.addVertex(d);
		graph.addVertex(e);
		graph.addVertex(f);
		graph.addEdge(ab);
		graph.addEdge(acd);
		graph.addEdge(de);
		graph.addEdge(bef);
	}
	
	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAddEdge() {
		graph.addEdge(acd);
		assertEquals(3, graph.getVertices().size()); // Make sure the vertexes were added
		Set<HyperEdge> edges = graph.getEdges(a);
		assertEquals(1, edges.size()); // There should be only one edge in the graph
		assertEquals(acd, edges.iterator().next()); // ...and it should be acd
		
		setUpGraph();
		assertEquals(4, graph.edges.size());
	}

	@Test
	public final void testRemoveEdge() {
		setUpGraph();
		graph.removeEdge(acd);
		Set<HyperEdge> aEdges = graph.getEdges(a);
		assertEquals(1, aEdges.size()); // We started with 2 a-headed edges, so removing one leaves 1
		assertFalse(aEdges.contains(acd));
		Set<HyperEdge> dEdges = graph.getEdges(d);
		assertEquals(1, dEdges.size()); // We started with 2 d-linked edges (1 head, 1 tail), so removing one leaves 1
		assertFalse(aEdges.contains(acd));
	}

	@Test
	public final void testAddVertex() {
		graph.addVertex(a);
		assertTrue(graph.getVertices().contains(a));
	}

	@Test
	public final void testRemoveVertex() {
		setUpGraph();
		graph.removeVertex(b);
		Set<Vertex> remainingVertexes = graph.getVertices();
		assertEquals(4, remainingVertexes.size()); // We started with 4 vertexes, so removing one leaves 4
		assertFalse(remainingVertexes.contains(b));
		
		Set<HyperEdge> edgesFromB = graph.getEdges(b);
		assertEquals(0, edgesFromB.size()); // Since we removed the b vertex, all edges with b as source should be gone.

		Set<HyperEdge> edgesFromA = graph.getEdges(a);
		assertEquals(1, edgesFromA.size()); // Since we removed the b vertex, all edges with b as target should be gone.
	}

	@Test
	public final void testGetVertexes() {
		setUpGraph();
		assertEquals(5, graph.getVertices().size());
	}

	@Test
	public final void testGetEdges() {
		setUpGraph();
		assertEquals(2, graph.getEdges(a).size());
	}

	@Test
	public final void testHyperedgeContainment() {
		setUpGraph();
		assertTrue(ab.getHeads().contains(a));
		assertTrue(ab.getTails().contains(b));
	}

}
