package io.bigr.graphtest;


public class EdgeImpl extends Edge {
	
	protected String name;
	
	public EdgeImpl (String name, Vertex source, Vertex target) {
		this.name = name;
		setTail(source);
		setHead(target);
	}

	public String getName() {
		return name;
	}
	
	public EdgeImpl setName(String name) {
		this.name = name;
		return this;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
