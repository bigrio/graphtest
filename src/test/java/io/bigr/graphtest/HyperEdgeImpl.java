package io.bigr.graphtest;


public class HyperEdgeImpl extends HyperEdge {
	
	protected String name;
	
	public HyperEdgeImpl (String name) {
		this.name = name;
	}
	
	public HyperEdgeImpl addHead(Vertex head) {
		this.heads.add(head);
		return this;
	}

	public HyperEdgeImpl addTail(Vertex tail) {
		this.tails.add(tail);
		return this;
	}
	
	public String getName() {
		return name;
	}
	
	public HyperEdgeImpl setName(String name) {
		this.name = name;
		return this;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
