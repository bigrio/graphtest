package io.bigr.graphtest;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a graph made of edges and vertexes (vertices). This 
 * implementation supports directional and parallel edges - an asymmetric 
 * directed graph. This implementation is not thread-safe.
 */
public class Graph<E extends Edge> extends HyperGraph<E> {

	@Override
	public void addEdge(E edge) {
		addVertex(edge.getTail());
		addVertex(edge.getHead());
		edges.add(edge);
	}

	/**
	 * @param vertex The source vertex
	 * @return All edges that originate from the source vertex
	 */
	@Override
	public Set<E> getEdges(Vertex source) {
		Set<E> result = new HashSet<E>();
		for (E e : edges) {
			if (source.equals(e.getTail())) {
				result.add(e);
			}
		}
		return result;
	}
}
