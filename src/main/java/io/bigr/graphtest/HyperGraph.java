package io.bigr.graphtest;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a digraph made of hyperedges and vertexes (vertices). 
 */
public class HyperGraph<E extends HyperEdge> {

	protected Set<Vertex> vertices = new HashSet<Vertex>();
	protected Set<E> edges = new HashSet<E>();
	
	/**
	 * Add an edge to the graph. If either the source or target vertex do not 
	 * yet exist in the graph they will be added.
	 * 
	 * @param edge The edge
	 */
	public void addEdge(E edge) {
		for (Vertex v : edge.getTails()) {
			addVertex(v);
		}
		
		for (Vertex v : edge.getHeads()) {
			addVertex(v);
		}
		
		edges.add(edge);
	}

	/**
	 * Add a vertex to the graph. If the vertex already exists within the 
	 * graph, does nothing.
	 * 
	 * @param vertex The vertex to add
	 */
	public void addVertex(Vertex vertex) {
		vertices.add(vertex);
	}
	
	/**
	 * Remove a vertex from the graph
	 * 
	 * @param vertex The vertex to remove
	 */
	public void removeVertex(Vertex vertex) {
		if (null == vertex) {
			return;
		}
		
		this.vertices.remove(vertex);
		
		// Remove all edges that were to or from the now-removed vertex
		Set<E> toRemove = new HashSet<E>();
		for (E e : edges) {
			if (e.getTails().contains(vertex) || e.getHeads().contains(vertex)) {
				toRemove.add(e);
			}
		}
		for (E e: toRemove) { // Second loop to avoid concurrent access error when removing from this.edges
			removeEdge(e);
		}
	}
	
	/**
	 * Remove an edge from the graph
	 * 
	 * @param edge
	 */
	public void removeEdge(E edge){
		edges.remove(edge);
	}
	
	/**
	 * @return All vertices registered in the graph
	 */
	public Set<Vertex> getVertices() {
		return vertices;
	}
	
	/**
	 * @param root The root vertex
	 * @return All edges that have the root as a head
	 */
	public Set<E> getEdges(Vertex root) {
		Set<E> result = new HashSet<E>();
		for (E e : edges) {
			if (e.getHeads().contains(root)) {
				result.add(e);
			}
		}
		return result;
	}
	
}
