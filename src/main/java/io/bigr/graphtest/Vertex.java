package io.bigr.graphtest;

/**
 * A node in a graph
 */
public interface Vertex {

	/**
	 * @return the name
	 */
	public abstract String getName();

}