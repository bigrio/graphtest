package io.bigr.graphtest;

import java.util.ArrayList;
import java.util.List;

/**
 * (Hyper)graph search algorithms
 */
public class Search {
	
	/**
	 * Perform a comprehensive search of a graph. This method will return all 
	 * valid paths. This method assumes a single valid start and target vertex.
	 * 
	 * @param startVertex The vertex at which the search should start
	 * @param targetVertex The vertex at which the search should end
	 * @param graph The graph over which to search
	 * @return
	 */
	public static <E extends Edge> List<List<E>> search(Vertex startVertex, Vertex targetVertex, Graph<E> graph) {
		// TODO implement
		
		return new ArrayList<List<E>>();
	}
}
