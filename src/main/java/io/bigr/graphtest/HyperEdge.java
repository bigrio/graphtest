package io.bigr.graphtest;

import java.util.HashSet;
import java.util.Set;

/**
 * A transition between N vertices.
 */
public abstract class HyperEdge {

	protected Set<Vertex> tails = new HashSet<Vertex>();
	protected Set<Vertex> heads = new HashSet<Vertex>();
	protected int score = 1; // Default score of 1 
	
	public Set<Vertex> getTails() {
		return tails;
	}
	
	public Set<Vertex> getHeads() {
		return heads;
	}
	
	/**
	 * @return the name
	 */
	public abstract String getName();

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * (result + this.heads.hashCode() + this.tails.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HyperEdge))
			return false;
		HyperEdge other = (HyperEdge) obj;
		return (this.getHeads().equals(other.getHeads()) &&
				this.getTails().equals(other.getTails()) && 
				this.getName().equals(other.getName()));
	}

	/**
	 * Get a score to use for weighted searches
	 * 
	 * @return the score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Set a score to use for weighted searches
	 * 
	 * @param score the score to set
	 */
	public void setScore(int score) {
		this.score = score;
	}

}
