package io.bigr.graphtest;

/**
 * A transition between two vertices. Note this is not a generic edge, but is 
 * specifically directed. An extra layer of abstraction (non-directed edge) can
 * be placed between this and HyperEdge should need arise.
 */
public abstract class Edge extends HyperEdge {

	/**
	 * @return the tail
	 */
	public Vertex getTail() {
		// For this implementation, we enforce single-member sets
		return getTails().iterator().next(); 
	}
	
	/**
	 * @param tail the tail to set
	 */
	public void setTail(Vertex tail) {
		this.tails.clear();
		tails.add(tail);
	}
	
	/**
	 * @return the head
	 */
	public Vertex getHead() {
		// For this implementation, we enforce single-member sets
		return getHeads().iterator().next(); 
	}
	
	/**
	 * @param head the head to set
	 */
	public void setHead(Vertex head) {
		this.heads.clear();
		heads.add(head);
	}
}